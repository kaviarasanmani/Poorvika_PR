from rest_framework import serializers

from .models import Purchase_order,Item

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields =(
            'title',
            'quantity',
            'unit_price',
            'net_amount',
            'gst',

        )
        

class PurchaseorderSerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True)
    class Meta:
        model = Purchase_order

        fields=(
                    'id',
                    'vendor',
                    'sender_reference',
                    'gross_amount',
                    'gst_amount',
                    'net_amount',
                    'items',
                    'branches',

        )
        # depth = 1


    def create(self, validated_data):
        item_data = validated_data.pop('items')
        purchase_order = Purchase_order.objects.create(**validated_data)
        for data in item_data:
            Item.objects.create(Purchase_order=purchase_order,**data)
        return purchase_order





    def update(self, instance, validated_data):
        items =validated_data.pop('items')

        item_list = list(instance.items.all())

        instance.sender_reference = validated_data.get('sender_reference',instance.sender_reference)
        instance.save()


        for data in items:
            items = item_list.pop(0)
            items.title = data.get('title',items.title)
            items.quantity = data.get('quantity',items.quantity)
            items.unit_price = data.get('unit_price',items.unit_price)
            items.net_amount = data.get('net_amount',items.net_amount)
            items.gst = data.get('gst',items.gst)
            items.save()
        return instance


