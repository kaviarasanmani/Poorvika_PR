from django.db import models

from branchers.models import branches
from vendors.models import vendor
# Create your models here.

class Release_order(models.Model):
    ro_date = models.DateField()
    Add_type = models.CharField(max_length=255)
    Size = models.CharField(max_length=255)
    vendor = models.ForeignKey(vendor, on_delete=models.CASCADE)
    color = models.CharField(max_length=255)
    gross_amount = models.DecimalField(max_digits=10,decimal_places=2)
    gst = models.IntegerField(default=1)
    gst_amount = models.DecimalField(max_digits=10,decimal_places=2)
    net_amunt = models.DecimalField(max_digits=10,decimal_places=2)
    billing_address = models.ForeignKey(branches, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.ro_date},{self.Add_type}'
    



class Pub_date(models.Model):
    pub_date = models.DateField(null=True,blank=True)
    ro = models.ForeignKey(Release_order,related_name="pub_date", on_delete=models.CASCADE)


    def __str__(self):
        return f'{self.pub_date},{self.ro.client}'



class Edition(models.Model):
    edition = models.CharField(max_length=200,null=True,blank=True)
    ro = models.ForeignKey(Release_order,related_name="edition", on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.edition},{self.ro}'