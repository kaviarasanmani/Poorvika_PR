from rest_framework.response import Response
from rest_framework import generics
from rest_framework import permissions
from django.views.decorators import csrf

from .models import Item,Purchase_order
from .serializers import PurchaseorderSerializer,ItemSerializer


class PoView(generics.ListCreateAPIView):
    queryset = Purchase_order.objects.all()
    serializer_class = PurchaseorderSerializer

class PoCreateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Purchase_order.objects.all()
    serializer_class = PurchaseorderSerializer



    # def get_permissions(self):
    #     if self.request.method in ['GET','PUT','DELETE']:
    #         return [permissions.IsAdminUser()]
    #     return[permissions.IsAuthenticated()]
