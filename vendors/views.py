from rest_framework.response import Response
from rest_framework import generics
from .models import vendor
from .serializers import VendorSerializer


class VendorView(generics.ListCreateAPIView):
    queryset= vendor.objects.all()
    serializer_class = VendorSerializer

class vendorCreateview(generics.RetrieveUpdateDestroyAPIView):
    queryset= vendor.objects.all()
    serializer_class = VendorSerializer