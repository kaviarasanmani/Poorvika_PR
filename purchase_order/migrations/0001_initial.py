# Generated by Django 4.0.1 on 2022-04-08 09:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('vendors', '0002_vendor_modified_at'),
    ]

    operations = [
        migrations.CreateModel(
            name='Purchase_order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sender_reference', models.CharField(blank=True, max_length=255, null=True)),
                ('gross_amount', models.DecimalField(decimal_places=2, max_digits=6)),
                ('gst_amount', models.DecimalField(decimal_places=2, max_digits=6)),
                ('net_amount', models.DecimalField(decimal_places=2, max_digits=6)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='invoices', to='vendors.vendor')),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('quantity', models.IntegerField(default=1)),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=6)),
                ('net_amount', models.DecimalField(decimal_places=2, max_digits=6)),
                ('gst', models.IntegerField(default=0)),
                ('Purchase_order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='purchase_order.purchase_order')),
            ],
        ),
    ]
