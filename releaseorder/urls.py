from django.urls import path,include
from.views import RoView,RoCreateView


urlpatterns = [
    path('ro',RoView.as_view(),name ='home'),
    path('ro/<pk>',RoCreateView.as_view(),name='create'),
]
