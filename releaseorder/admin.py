from django.contrib import admin

# Register your models here.
from .models import Release_order,Pub_date,Edition

admin.site.register(Release_order),
admin.site.register(Pub_date),
admin.site.register(Edition),
