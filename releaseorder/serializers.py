from rest_framework import serializers


from.models import Edition,Release_order,Pub_date

class edition_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Edition
        fields =(
            # 'ro',   
            'edition',
        )
        depth = 1
class Pub_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Pub_date
        fields = (
            'pub_date',
        )
        depth = 1

class realeaseSerializer(serializers.ModelSerializer):
    edition = edition_Serializer(many=True)
    pub_date = Pub_Serializer(many=True)
    class Meta:
        model = Release_order
        read_only_field =(
            
            'billing_address',  
        )
        fields =(
            'id',
            'ro_date',
            'Add_type',
            'Size',
            'vendor',
            'color',
            'gross_amount',
            'gst',
            'gst_amount',
            'net_amunt',
            'billing_address',   
            'edition',
            'pub_date',
        )
        depth = 1

    def create(self, validated_data):
        edition_date = validated_data.pop('edition')
        pub_date = validated_data.pop('pub_date')

        ro=Release_order.objects.create(**validated_data)
        for data in edition_date:
            Edition.objects.create(ro=ro,**data)
        for data in pub_date:
            Pub_date.objects.create(ro=ro,**data)
        return ro



    # def update(self, instance, validated_data):
        
    #     edition_data = validated_data.pop('edition')
    #     edition = (instance.edition).all()
    #     edition = list(edition)
    #     instance.ro_date = validated_data.get('ro_date',instance.ro_date)
    #     instance.input1 = validated_data.get('input1',instance.input1)
    #     instance.input2 = validated_data.get('input2',instance.input2)
    #     instance.input3 = validated_data.get('input3',instance.input3)
    #     instance.input4 = validated_data.get('input4',instance.input4)

    #     instance.save()
        

    #     for edition_data in edition_data:
    #         editons = edition.pop(0)
    #         editons.edition = edition_data.get('name',editons.edition)
    #         editons.save()

    #     return instance
        

    def update(self, instance, validated_data):
        edition = validated_data.pop('edition')
        pub_date = validated_data.pop('pub_date')

        edition_list = list(instance.edition.all())
        pub_list = list(instance.pub_date.all())

        instance.ro_date = validated_data.get('ro_date',instance.ro_date)
        instance.save()


        for data in edition:
            edition = edition_list.pop(0)
            edition.edition = data.get('edition',edition.edition)
            edition.save()

        for data in pub_date:
            pub = pub_list.pop(0)
            pub.pub_date = data.get('pub_date',pub.pub_date)
            pub.save()

        return instance
